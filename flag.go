// Flag defines an address flag that can be specified by a hostname:port pair.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package address

import (
	"errors"
	"fmt"
	"os"
)

// Flag defines an address flag. This satisfies the pcastools/flag.Flag interface.
type Flag struct {
	name        string    // The name of the flag.
	description string    // The short-form help text for this flag.
	usage       string    // The long-form help text for this flag.
	addr        **Address // The address.
}

/////////////////////////////////////////////////////////////////////////
// Flag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (f *Flag) Name() string {
	if f == nil {
		return ""
	}
	return f.name
}

// Description returns a one-line description of this variable.
func (f *Flag) Description() string {
	if f == nil {
		return ""
	}
	return fmt.Sprintf("%s (default: \"%s\")", f.description, *f.addr)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *Flag) Usage() string {
	if f == nil {
		return ""
	}
	return f.usage
}

// Parse parses the string.
func (f *Flag) Parse(in string) error {
	if f == nil {
		return errors.New("uninitialised Flag")
	}
	addr, err := New(in)
	if err != nil {
		return err
	}
	*(f.addr) = addr
	return nil
}

// NewFlag represents an address flag with the given name, description, and usage string. It has backing variable addr, with default value defaultAddr.
func NewFlag(name string, addr **Address, defaultAddr *Address, description string, usage string) *Flag {
	*addr = defaultAddr
	return &Flag{
		name:        name,
		description: description,
		usage:       usage,
		addr:        addr,
	}
}

// EnvUsage returns a standard usage message for the address flag -f that reads its default value from the environment variable env.
func EnvUsage(f string, env string) string {
	var status string
	// read the environment variable and try to parse it
	val, ok := os.LookupEnv(env)
	if ok {
		_, err := New(val)
		if err == nil {
			status = fmt.Sprintf("current value \"%s\", which is valid", val)
		} else {
			status = fmt.Sprintf("current value \"%s\", which is invalid", val)
		}
	} else {
		status = "currently unset"
	}
	return fmt.Sprintf("The value of the flag -%s should either take the form \"hostname[:port]\" or be a web-socket URI \"ws://host/path\". The default value can be specified using the environment variable %s (%s).", f, env, status)
}
