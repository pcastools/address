// Address provides a uniform representation of tcp and websocket addresses.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package address

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

// Address represents an address of the form hostname:port or a websocket URI.
type Address struct {
	hasPort bool     // Is a port number set?
	port    int      // The port number
	u       *url.URL // The URI
}

/////////////////////////////////////////////////////////////////////////
// Address functions
/////////////////////////////////////////////////////////////////////////

// NewTCP returns a new address for the given tcp hostname and port number.
func NewTCP(hostname string, port int) (*Address, error) {
	return New(hostname + ":" + strconv.Itoa(port))
}

// New parses a hostname:port pair of the form "hostname[:port]" or "tcp://hostname[:port][/]", or a websocket URI of the form "ws://host/path". The returned address will have been validated.
func New(s string) (*Address, error) {
	// If necessary, add the tcp scheme
	if !strings.Contains(s, "://") {
		s = "tcp://" + s
	}
	// Attempt to parse s as a URI
	u, err := url.ParseRequestURI(s)
	if err != nil {
		return nil, err
	}
	// The URI shouldn't contain a username/password or query values
	if u.User != nil {
		return nil, errors.New("a username or password is not supported")
	} else if len(u.Query()) != 0 {
		return nil, errors.New("query values are not supported")
	}
	// Perform scheme-specific validation
	switch u.Scheme {
	case "ws":
		// The cannon have an emoty path component, or a dir component
		path := u.EscapedPath()
		if len(path) == 0 || path == "/" {
			return nil, errors.New("a websocket must have a path")
		} else if strings.HasSuffix(path, "/") {
			return nil, errors.New("a websocket must not have a path ending in /")
		}
	case "tcp":
		// This cannot have a path component
		path := u.EscapedPath()
		if len(path) != 0 && path != "/" {
			return nil, errors.New("a TCP connection must not have a path")
		}
	default:
		return nil, fmt.Errorf("unknown URI scheme: %s", u.Scheme)
	}
	// Create a new Address
	a := &Address{u: u}
	// Validate the port
	if portStr := u.Port(); len(portStr) != 0 {
		port, err := strconv.Atoi(portStr)
		if err != nil {
			return nil, fmt.Errorf("unable to parse port number: %w", err)
		} else if port < 1 || port > 65535 {
			return nil, fmt.Errorf("the port number (%d) must be between 1 and 65535", port)
		}
		a.hasPort = true
		a.port = port
	}
	return a, nil
}

// HasPort returns true iff a port was specified.
func (a *Address) HasPort() bool {
	return a != nil && a.hasPort
}

// Port returns the port number (this will be 0 if unspecified).
func (a *Address) Port() int {
	if a.HasPort() {
		return a.port
	}
	return 0
}

// Hostname returns the hostname.
func (a *Address) Hostname() string {
	if a == nil {
		return ""
	}
	return a.u.Hostname()
}

// HostnameAndPort returns "hostname[:port]".
func (a *Address) HostnameAndPort() string {
	str := a.Hostname()
	if a.HasPort() {
		str += ":" + strconv.Itoa(a.Port())
	}
	return str
}

// EscapedPath returns the escaped form of the path. In general there are multiple possible escaped forms of any path.
func (a *Address) EscapedPath() string {
	if a == nil {
		return "/"
	}
	return "/" + strings.TrimLeft(a.u.EscapedPath(), "/")
}

// Scheme returns the scheme.
func (a *Address) Scheme() string {
	if a == nil {
		return ""
	}
	return a.u.Scheme
}

// IsTCP returns true iff the address is a tcp address.
func (a *Address) IsTCP() bool {
	return a.Scheme() == "tcp"
}

// URI returns the address as a URI.
func (a *Address) URI() string {
	if a == nil {
		return ""
	}
	return a.Scheme() + "://" + a.HostnameAndPort() + a.EscapedPath()
}

// String returns a string representation of the address.
func (a *Address) String() string {
	if a.IsTCP() {
		return a.HostnameAndPort()
	}
	return a.URI()
}
